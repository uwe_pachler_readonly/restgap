# RestGap #
[![javadoc](https://javadoc.io/badge2/org.bitbucket.restgap/restgap/javadoc.svg)](https://javadoc.io/badge2/org.bitbucket.restgap/restgap) 


RestGap is a library which bridges the Gap between JAX-RS annotated interfaces and Spring's RestTemplate. When Using RestGap, you provide a JAX-RS annotated Java interface to the RestGap factory, along with a RestTemplate instance, and you'll get a HTTP client object that implements your JAX-RS interface.

Other than that, zero coding needed.

## Usage ##

[See the Wiki](https://bitbucket.org/restgap/restgap/wiki/Home) for details on how to use REST Gap.

## Maven Depencency ##
``` xml
<depencency>
    <groupId>org.bitbucket.restgap</groupId>
    <artifactId>restgap</artifactId>
    <version>0.1.0</version>
</dependency>
```