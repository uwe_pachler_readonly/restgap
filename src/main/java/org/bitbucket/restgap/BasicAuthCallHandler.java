/*
 * Copyright 2016 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap;

import org.bitbucket.restgap.client.CallHandler;
import java.nio.charset.Charset;
import java.util.Base64;
import java.util.Collections;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import org.bitbucket.restgap.client.RestCallContext;

/**
 *
 * @author count
 */
class BasicAuthCallHandler implements CallHandler {
	private static final Charset AUTHORIZATION_CHARSET = Charset.forName("ISO-8859-1");
	
	private final String authorization;

	public BasicAuthCallHandler(String username, String password) {
		String rawAuthentication = "Basic " + emptyForNull(username) + ':' + emptyForNull(password);
		authorization = Base64.getEncoder().encodeToString(rawAuthentication.getBytes(AUTHORIZATION_CHARSET));
	}
	
	
	@Override
	public CompletionStage<RestCallContext> handleCall(RestCallContext ctx) {
		ctx.getCall().getHeaders().put("Authorization", Collections.singletonList(authorization));
		return CompletableFuture.completedFuture(ctx);
	}

	private String emptyForNull(String s) {
		return s==null ? "" : s;
	}
	
}
