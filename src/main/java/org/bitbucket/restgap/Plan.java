/*
 * Copyright 2016 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap;

import java.lang.reflect.Type;
import java.util.Arrays;
import org.bitbucket.restgap.frontend.ResponseConverter;

/**
 *
 * @author count
 */
public class Plan {
	
	private final String httpMethod;
	private final String pathTemplate;
	private final Type responseType;
	private final ParameterDefinition[] parameterDefinitions;
	private final String[] consumedMediaTypes;
	private final String[] producedMediaTypes;
	private final ResponseConverter responseConverter;
	
	public Plan(String httpMethod, String pathTemplate, Type responseType, ParameterDefinition[] parameterDefinitions, ResponseConverter responseConverter, String[] producedMediaTypes, String[] consumedMediaTypes) {
		this.httpMethod = httpMethod;
		this.pathTemplate = pathTemplate;
		this.responseType = responseType;
		this.parameterDefinitions = parameterDefinitions;
		this.responseConverter = responseConverter;
		this.producedMediaTypes = Arrays.copyOf(producedMediaTypes, producedMediaTypes.length);
		this.consumedMediaTypes = Arrays.copyOf(consumedMediaTypes, consumedMediaTypes.length);
	}

	public String getHttpMethod() {
		return httpMethod;
	}

	public String getPathTemplate() {
		return pathTemplate;
	}

	public Type getResponseType() {
		return responseType;
	}

	public ParameterDefinition[] getParameterDefinitions() {
		return parameterDefinitions;
	}

	public ResponseConverter getResponseConverter() {
		return responseConverter;
	}

	public String[] getProducedMediaTypes() {
		return producedMediaTypes;
	}

	public String[] getConsumedMediaTypes() {
		return consumedMediaTypes;
	}
	
}
