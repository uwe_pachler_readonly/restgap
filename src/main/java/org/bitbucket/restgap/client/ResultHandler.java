/*
 * Copyright 2016 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.client;

import java.util.concurrent.CompletableFuture;
import org.bitbucket.restgap.backend.Backend;
import org.bitbucket.restgap.backend.RestCallResult;

/**
 * A ResultHandlers can modify the result that a RestGap client's
 * {@link Backend} produces before it is processed further. 
 * @author count
 */
public interface ResultHandler {
	
	/**
	 * Called right before the RestTemplate's result is processed further. Because
	 * a ResultHandler may take time to come to a decision, this method returns
	 * a {@link CompletableFuture}. For instance, a ResultHandler may try 
	 * new credentials when receiving a 401 response.
	 * @param context	the result produced by the call (or by a handler further
	 *	up in the chain)
	 * @return	the handler can return the same result, or a new one if it
	 *	intends to change the result of the call (ignored when {@link RestCallContext#abort() }
	 *	or {@link RestCallContext#reissue() } were called)
	 */
	CompletableFuture<RestCallContext> handleResult(RestCallContext context);
}
