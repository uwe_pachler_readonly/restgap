/*
 * Copyright 2016 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.client;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import org.bitbucket.restgap.Plan;

/**
 * When the created client instance has a {@link CallHandler} installed,
 * the handler's {@link CallHandler#handleCall(RestCallContext)} method will be called before
 * the actual RestTemplate method is invoked. A <code>RestCall</code> instance
 * contains selected call parameters for the RestTemplate call, which the
 * call handler may modify before the RestTemplate instance is invoked.
 * @author count
 */
public interface RestCall {
	
	Plan getPlan();
	
	/**
	 * @return	the HttpEntity to make the REST call. Provides access to HTTP
	 *	headers and request body.
	 */
	Object getHttpBody();
	
	void setHttpBody(Object body);
	
	/**
	 * @return	the HTTP method that will be used to call the resource.
	 */
	String getHttpMethod();
	
	/**
	 * Set a new HttpMethod to call the REST resource. Use with care.
	 * @param httpMethod	the new HttpMethod that should be used instead to
	 *	call the resource.
	 */
	void setHttpMethod(String httpMethod);

	String getUri();

	void setUri(String uri);
	
	Map<String,List<Object>> getHeaders();

	Map<String, Object> getPathParams();

	Type getResponseType();

	Map<String, List<Object>> getQueryParams();

	Map<String, List<Object>> getFormParams();
	
}
