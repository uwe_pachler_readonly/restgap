/*
 * Copyright 2017 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.client;

import org.bitbucket.restgap.backend.RestCallResult;

/**
 * An instance of this call is created by the client's invocation handler for
 * each invocation of a client method. It is then passed to any {@link CallHandler}
 * or {@link ResultHandler} 
 * @author upachler
 */
public interface RestCallContext {
	
	RestCall getCall();
	
	RestCallResult getResult();
	
	Object getProperty(String name);
	void setProperty(String name, Object value);
	
	void reissue();
	
	boolean isReissueRequested();
	
	void abort();

	void setResult(RestCallResult r);

	boolean isAbortRequested();
	
	void clearAlternateFlow();
}
