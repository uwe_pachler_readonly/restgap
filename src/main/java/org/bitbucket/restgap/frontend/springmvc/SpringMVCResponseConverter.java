/*
 * Copyright 2017 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.frontend.springmvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.bitbucket.restgap.Plan;
import org.bitbucket.restgap.backend.RestCallResult;
import org.bitbucket.restgap.frontend.ResponseConverter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

/**
 *
 * @author upachler
 */
public class SpringMVCResponseConverter implements ResponseConverter{
	
	private final Function<RestCallResult, Object> converterFunction;

	public SpringMVCResponseConverter(Function<RestCallResult, Object> converterFunction) {
		this.converterFunction = converterFunction;
	}

	
	@Override
	public Object convert(RestCallResult e, Plan plan) {
		byte[] body = e.getBody()!=null && byte[].class.isAssignableFrom(e.getBody().getClass()) ? (byte[]) e.getBody() : null;
		int statusCode = e.getStatusCode();

		if (statusCode / 100 == 2) {
			return converterFunction.apply(e);
		}

		HttpHeaders headers = e.getHeaders().entrySet().stream()
			.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, SpringMVCResponseConverter::listMerge, HttpHeaders::new));

		HttpStatus status = HttpStatus.valueOf(statusCode);
		if (statusCode / 100 == 4) { // 4xx
			throw new HttpClientErrorException(status, "", headers, body, null);
		} else {
			throw new HttpServerErrorException(status, "", headers, body, null);
		}
	}

	private static <T> List<T> listMerge(List<T> l1, List<T> l2) {
		List<T> r = new ArrayList(l1);
		r.addAll(l2);
		return r;
	}
	
}
