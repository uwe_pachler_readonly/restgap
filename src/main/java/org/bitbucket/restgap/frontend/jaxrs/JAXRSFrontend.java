/*
 * Copyright 2016 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.frontend.jaxrs;

import org.bitbucket.restgap.impl.PlanUtils;
import org.bitbucket.restgap.*;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.GET;
import javax.ws.rs.HEAD;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.bitbucket.restgap.backend.RestCallResult;
import org.bitbucket.restgap.frontend.Frontend;
import org.bitbucket.restgap.frontend.ResponseConverter;

/**
 * This factory creates a map from method signature strings to {@link Plan}s.
 * It analyses a given class and constructs {@link Plan} instances for each
 * method that carries JAX RS annotations.
 */
public final class JAXRSFrontend implements Frontend {
	
	static final Frontend INSTANCE = new JAXRSFrontend();
	
	static final String[] WILDCARD_MEDIA_TYPES = { MediaType.WILDCARD };
	
	public static Frontend instance() {
		return INSTANCE;
	}
	
	@Override
	public Map<String, Plan> createPlans(Class<?> clazz, String baseUri) {
		
		
		Path path = clazz.getAnnotation(Path.class);
		
		String baseResourcePath = appendPath(baseUri, path);
		
		Map<String,Plan> plans = new HashMap<>();
		for(Method m : clazz.getDeclaredMethods()) {
			
			Plan plan = mkPlan(baseResourcePath, m);
			plans.put(m.toGenericString(), plan);
		}
		return plans;
	}
	
	private static String appendPath(String baseUri, Path path) {
		return PlanUtils.appendPath(baseUri, path!=null ? path.value() : null);
	}
	
	private static String mapHttpMethod(Method m) {
		if(null != m.getAnnotation(POST.class)) {
			return "POST";
		} else if(null != m.getAnnotation(DELETE.class)) {
			return "DELETE";
		} else if(null != m.getAnnotation(OPTIONS.class)) {
			return "OPTIONS";
		} else if(null != m.getAnnotation(GET.class)) {
			return "GET";
		} else if(null != m.getAnnotation(HEAD.class)) {
			return "HEAD";
		} else if(null != m.getAnnotation(PUT.class)) {
			return "PUT";
		} else {
			javax.ws.rs.HttpMethod httpMethod = m.getAnnotation(javax.ws.rs.HttpMethod.class);
			if(httpMethod == null) {
				return null;
			}
			
			return httpMethod.value();
		}
	}
	
	
	
	private static Plan mkPlan(String basePath, Method m) {
		final String httpMethod = mapHttpMethod(m);
		
		Path path = m.getAnnotation(Path.class);
		final String pathTemplate = appendPath(basePath, path);
		
		Produces produces = m.getAnnotation(Produces.class);
		String[] producedMediaTypes = produces != null
			?	produces.value()
			:	WILDCARD_MEDIA_TYPES;
		
		Consumes consumes = m.getAnnotation(Consumes.class);
		String[] consumedMediaTypes = consumes != null
			?	consumes.value()
			:	WILDCARD_MEDIA_TYPES;
		
		List<ParameterDefinition> pdefs = new ArrayList<>();
		
		Function<RestCallResult,Object> converterFunction;
		final Type responseType;
		Type returnType = m.getGenericReturnType();
		if(Void.class == returnType) {
			responseType = null;
			converterFunction = x -> null;
		} else if(returnType instanceof Class || returnType instanceof ParameterizedType){
			responseType = returnType;
			converterFunction = RestCallResult::getBody;
		} else {
			throw new UnsupportedOperationException(String.format("Unsupported return type %s in method %s", returnType.toString(), m.getName()));
		}
		
		Annotation[][] annotations = m.getParameterAnnotations();
		Class[] types = m.getParameterTypes();
		for(int i=0; i<annotations.length; ++i) {
			PathParam pathParam = PlanUtils.getAnnotation(annotations[i], PathParam.class);
			QueryParam queryParam = PlanUtils.getAnnotation(annotations[i], QueryParam.class);
			FormParam formParam = PlanUtils.getAnnotation(annotations[i], FormParam.class);
			HeaderParam headerParam = PlanUtils.getAnnotation(annotations[i], HeaderParam.class);
			CookieParam cookieParam = PlanUtils.getAnnotation(annotations[i], CookieParam.class);
			MatrixParam matrixParam = PlanUtils.getAnnotation(annotations[i], MatrixParam.class);
			
			if(pathParam != null) {
				pdefs.add(new ParameterDefinition(ParamKind.PATH, pathParam.value(), i));
			} else if(queryParam!=null) {
				ParamKind paramKind;
				if(List.class.isAssignableFrom(types[i]) || Set.class.isAssignableFrom(types[i])) {
					paramKind = ParamKind.QUERY_COLLECTION;
				} else {
					paramKind = ParamKind.QUERY;
				}
				pdefs.add(new ParameterDefinition(paramKind, queryParam.value(), i));
			} else if(formParam!=null) {
				pdefs.add(new ParameterDefinition(ParamKind.FORM, formParam.value(), i));
			} else if(headerParam!=null) {
				pdefs.add(new ParameterDefinition(ParamKind.HEADER, headerParam.value().toLowerCase(), i));
			} else if(cookieParam!=null || matrixParam!=null) {
				throw new UnsupportedOperationException("CookieParam and MatrixParam are not yet supported");
			} else {
				pdefs.add(new ParameterDefinition(ParamKind.ENTITY, null, i));
			}
		}
		
		final ParameterDefinition[] parameterDefinitions = pdefs.toArray(new ParameterDefinition[pdefs.size()]);
		
		ResponseConverter converter = new JAXRSResponseConverter(converterFunction);
		return new Plan(httpMethod, pathTemplate, responseType, parameterDefinitions, converter, producedMediaTypes, consumedMediaTypes);
	}
	
	
}
