/*
 * Copyright 2016 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.frontend.jaxrs;

import java.lang.annotation.Annotation;
import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import javax.ws.rs.core.EntityTag;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Link;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import org.bitbucket.restgap.backend.RestCallResult;

/**
 *
 * @author count
 */
class JAXRSResponseEntityAdapter extends Response {
	
	private static final Annotation[] EMPTY_ANNOTATIONS_ARRAY = {};
	
	private final RestCallResult responseEntity;

	public JAXRSResponseEntityAdapter(RestCallResult responseEntity) {
		this.responseEntity = responseEntity;
	}
	
	
	@Override
	public int getStatus() {
		return responseEntity.getStatusCode();
	}

	@Override
	public StatusType getStatusInfo() {
		return Response.Status.fromStatusCode(getStatus());
	}

	@Override
	public Object getEntity() {
		return responseEntity.getBody();
	}

	@Override
	public boolean hasEntity() {
		return responseEntity.getBody() != null;
	}

	@Override
	public MediaType getMediaType() {
		String mt = getOneHeader("Content-Type");
		if(mt == null) {
			return null;
		} else {
			return MediaType.valueOf(mt);
		}
	}

	@Override
	public Locale getLanguage() {
		String l = getOneHeader("Content-Language");
		return Locale.forLanguageTag(l);
	}
	
	@Override
	public URI getLocation() {
		return URI.create(getOneHeader("Location"));
	}

	@Override
	public MultivaluedMap<String, String> getStringHeaders() {
		MultivaluedMap<String,String> mvm = new MultivaluedHashMap<>();
		for(Map.Entry<String, List<String>> e : responseEntity.getHeaders().entrySet()) {
			mvm.put(e.getKey(), e.getValue());
		}
		return mvm;
	}

	@Override
	public MultivaluedMap<String, Object> getHeaders() {
		MultivaluedMap<String,Object> mvm = new MultivaluedHashMap<>();
		for(Map.Entry<String, List<String>> e : responseEntity.getHeaders().entrySet()) {
			mvm.put(e.getKey(), new ArrayList<>(e.getValue()));
		}
		return mvm;
	}

	
	@Override
	public <T> T readEntity(Class<T> entityType) {
		return readEntity(entityType, EMPTY_ANNOTATIONS_ARRAY);
	}

	@Override
	public <T> T readEntity(GenericType<T> entityType) {
		return readEntity(entityType, EMPTY_ANNOTATIONS_ARRAY);
	}

	@Override
	public <T> T readEntity(Class<T> clazz, Annotation[] annotations) {
		return readEntity(new GenericType<>(clazz), annotations);
	}

	@Override
	public <T> T readEntity(GenericType<T> genericType, Annotation[] annotations) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public boolean bufferEntity() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void close() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public int getLength() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Set<String> getAllowedMethods() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Map<String, NewCookie> getCookies() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public EntityTag getEntityTag() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Date getDate() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Date getLastModified() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Set<Link> getLinks() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public boolean hasLink(String relation) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Link getLink(String relation) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Link.Builder getLinkBuilder(String relation) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public MultivaluedMap<String, Object> getMetadata() {
		return this.getHeaders();
	}

	@Override
	public String getHeaderString(String name) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	private String getOneHeader(String headerName) {
		List<String> values = responseEntity.getHeaders().get(headerName);
		if(values==null || values.isEmpty()) {
			return null;
		} else {
			return values.get(0);
		}
	}
}
