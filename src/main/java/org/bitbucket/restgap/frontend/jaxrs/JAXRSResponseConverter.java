/*
 * Copyright 2016 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.frontend.jaxrs;

import java.lang.reflect.Type;
import java.util.function.Function;
import org.bitbucket.restgap.frontend.ResponseConverter;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.NotAcceptableException;
import javax.ws.rs.NotAllowedException;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.NotSupportedException;
import javax.ws.rs.RedirectionException;
import javax.ws.rs.ServerErrorException;
import javax.ws.rs.ServiceUnavailableException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.bitbucket.restgap.Plan;
import org.bitbucket.restgap.backend.RestCallResult;

/**
 *
 * @author count
 */
class JAXRSResponseConverter implements ResponseConverter{

	private final Function<RestCallResult,Object> converterFunction;
	
	JAXRSResponseConverter(Function<RestCallResult,Object> converterFunction) {
		this.converterFunction = converterFunction;
	}
	@Override
	public Object convert(RestCallResult e, Plan plan) {
		// check for conditions that cause exceptions to be thrown
		Type rtype = plan.getResponseType();
		boolean returnTypeIsJAXRSResponse = (rtype instanceof Class && ((Class)rtype).isAssignableFrom(Response.class));
		if(!returnTypeIsJAXRSResponse) {
			check(e);
		}
		return converterFunction.apply(e);
	}
	
	/**
	 * Checks if the given ResponseEntity represents a response that generates
	 * JAX-RS exceptions in the JAX-RS runtime.
	 * The method either terminates normally or throws an exception matching
	 * the HTTP status code (when 3xx, 4xx and 5xx).
	 * @param e	the ResponseEntity to check.
	 * @throws WebApplicationException	this exception or one of its subclasses
	 *	are thrown when the HTTP status code is in the 3xx, 4xx or 5xx group.
	 */
	private void check(RestCallResult e) throws WebApplicationException{
		int statusCode = e.getStatusCode();
		Status s = Status.fromStatusCode(statusCode);
		if(s!=null) {
			switch(s) {
				case BAD_REQUEST:
					throw new BadRequestException(toResponse(e));
				case FORBIDDEN:
					throw new ForbiddenException(toResponse(e));
				case NOT_ACCEPTABLE:
					throw new NotAcceptableException(toResponse(e));
				case METHOD_NOT_ALLOWED:
					throw new NotAllowedException(toResponse(e));
				case UNAUTHORIZED:
					throw new NotAuthorizedException(toResponse(e));
				case NOT_FOUND:
					throw new NotFoundException(toResponse(e));
				case UNSUPPORTED_MEDIA_TYPE:
					throw new NotSupportedException(toResponse(e));
				case SERVICE_UNAVAILABLE:
					throw new ServiceUnavailableException(toResponse(e));
			}
		}
		
		switch (statusCode / 100) {
			case 3: // 3xx
				throw new RedirectionException(toResponse(e));
			case 4: // 4xx
				throw new ClientErrorException(toResponse(e));
			case 5: // 5xx
				throw new ServerErrorException(toResponse(e));
		}
	}

	private Response toResponse(RestCallResult e) {
		if(e.getBody() instanceof Response) {
			return (Response)e.getBody();
		} else {
			return new JAXRSResponseEntityAdapter(e);
		}
	}
	
}
