/*
 * Copyright 2016 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap;

import javax.ws.rs.Path;
import org.bitbucket.restgap.backend.spring.RestTemplateBackend;
import org.bitbucket.restgap.frontend.Frontend;
import org.bitbucket.restgap.frontend.jaxrs.JAXRSFrontend;
import org.springframework.web.client.RestTemplate;

/**
 * This factory bridges the gap between JAX RS and Spring's {@link RestTemplate}
 * by creating fully functional REST clients that implement a given JAX RS annotated Java
 * interface. When one of the interface's JAX RS annotated methods is called,
 * that call is translated to a RestTemplate method call.
 */
public final class RESTTemplateJAXRSClientFactory {
	
	//prevent instantiation
	private RESTTemplateJAXRSClientFactory() {
	}
	
	/**
	 * Creates a new REST client which implements the given <a href='https://jax-rs-spec.java.net/'>JAX-RS</a>
	 * annotated Java interface. 
	 * The new client will use the given {@link RestTemplate} 
	 * to make HTTP calls. All {@link Path} annotations in the interface
	 * will be resolved against the given base URI, so that for a base URI {@code http://foo/}
	 * and a client annotated with {@code @Path("/bar")}, the URI for that client is 
	 * resolved as {@code http://foo/bar}.<p>
	 * Note that it is an error to pass non-interface {@link Class} objects.
	 * @param <T>	Actual type the client will implement
	 * @param restTemplate	{@link RestTemplate} that the new client will use to make HTTP calls
	 * @param baseUri	The base URI that the client will use.
	 * @param clazz	The class object for the Java interface {@code T} that the new client
	 *	will implement.
	 * @return	A new REST client implementing {@code T}.
	 */
	public static <T> T create(RestTemplate restTemplate, String baseUri, Class<T> clazz) {
		return create(restTemplate, baseUri, clazz, null, null);
	}
	
	/**
	 *  Creates a new REST client which implements the given <a href='https://jax-rs-spec.java.net/'>JAX-RS</a>
	 * annotated Java interface, using HTTP Basic Authentication.
	 * This method works the same as {@link #create(RestTemplate, String, Class) },
	 * but adds a <a href='https://tools.ietf.org/html/rfc7617'>HTTP Basic Authentication</a> header
	 * to each REST call. As a general rule for HTTP Basic Authentication, we 
	 * recommend to only use this method over TLS 1.2+ (https) connections, as
	 * credentials are transmitted as de-facto clear text.
	 * @param <T>	Actual type the client will implement
	 * @param restTemplate	{@link RestTemplate} that the new client will use to make HTTP calls
	 * @param baseUri	The base URI that the client will use.
	 * @param clazz	The class object for the Java interface {@code T} that the new client
	 *	will implement.
	 * @param username	The username to use in the Authorization header.
	 * @param password	The password to use in the Authorization header.
	 * @return	A new REST client implementing {@code T}.
	 */
	public static <T> T create(RestTemplate restTemplate, String baseUri, Class<T> clazz, String username, String password) {
		
		ClientFactory<T,RestTemplate> cf = ClientFactory.ofInterface(clazz)
			.frontend(JAXRSFrontend.instance())
			.baseUri(baseUri)
			.backend(RestTemplateBackend.instance())
			.build();
		
		return cf.create(restTemplate, username, password);
	}
	
}
