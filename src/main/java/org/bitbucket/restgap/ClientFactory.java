/*
 * Copyright 2017 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap;

import java.lang.reflect.Proxy;
import java.util.Map;
import org.bitbucket.restgap.backend.Backend;
import org.bitbucket.restgap.backend.PlanExecutor;
import org.bitbucket.restgap.frontend.Frontend;

/**
 *
 * @author upachler
 */
public class ClientFactory<T,F> {
	
	public interface PartialBuilder<T> {
		PartialBuilder<T> frontend(Frontend frontend);
		PartialBuilder<T> baseUri(String baseUri);
		<F> CompleteBuilder<T,F> backend(Backend<F> backend);
		
	}
	
	public interface CompleteBuilder<T,F> {
		ClientFactory<T,F> build();
	}
	
	static class BuilderImpl<T> implements PartialBuilder<T> {
		Class<T> clazz;
		Frontend frontend;
		String baseUri;

		public BuilderImpl(Class<T> clazz) {
			this.clazz = clazz;
		}
		
		@Override
		public PartialBuilder<T> frontend(Frontend frontend) {
			this.frontend = frontend;
			return this;
		}

		@Override
		public PartialBuilder<T> baseUri(String baseUri) {
			this.baseUri = baseUri;
			return this;
		}

		@Override
		public <F> CompleteBuilder<T, F> backend(final Backend<F> backend) {
			return new CompleteBuilder<T, F>() {
				@Override
				public ClientFactory<T,F> build() {
					return ClientFactory.<T,F>build(BuilderImpl.this, backend);
				}
			};
		}
		
	}
	
	public static <T> PartialBuilder<T> ofInterface(Class<T> clazz) {
		return new BuilderImpl(clazz);
	}
	
	private static <T,F> ClientFactory<T,F> build(BuilderImpl<T> b, final Backend<F> backend) {
		
		final Map<String,Plan> plans = b.frontend.createPlans(b.clazz, b.baseUri);
		final Class clazz = b.clazz;
		final ClassLoader cl = clazz.getClassLoader();

		return new ClientFactory<T, F>(plans, clazz, backend, cl);
	}
	
	final Map<String,Plan> plans;
	final Class<T> clazz;
	final ClassLoader cl;
	final Backend backend;

	private ClientFactory(Map<String, Plan> plans, Class<T> clazz, Backend backend, ClassLoader cl) {
		this.plans = plans;
		this.clazz = clazz;
		this.cl = cl;
		this.backend = backend;
	}
	
	public T create(F clientImplementation) {
		return create(clientImplementation, null, null);
	}
	
	public T create(F clientImplementation, String username, String password) {
		PlanExecutor executor = backend.createPlanExecutor(clientImplementation);
		PlanInvocationHandler handler = new PlanInvocationHandler(clazz, executor, plans);
		if(username!=null || password!=null) {
			handler.addCallHandler(new BasicAuthCallHandler(username, password));
		}
		return clazz.cast(Proxy.newProxyInstance(cl, new Class[]{clazz}, handler));
	}
}
