/*
 * Copyright 2016 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap;

import java.util.Collection;
import java.util.Map;

/**
 *
 * @author count
 */
 public enum ParamKind {
	/**
	 * Parameter maps a HTTP Header
	 */
	HEADER, 
	/**
	 * Parameter maps the HTTP entity body
	 */
	ENTITY, 
	/**
	 * Parameter maps a single URI query parameter
	 */
	QUERY, 
	/**
	 * Parameter maps a Java {@link Collection}, except {@link Map}
	 */
	QUERY_COLLECTION, 
	/**
	 * Parameter maps a set of query parameters (must be given as Map&lt;String,?&gt;)
	 */
	QUERY_MAP, 
	/**
	 * Parameter maps a set of query parameters (must be given as Map&lt;String,? extends Collection&gt;)
	 */
	QUERY_MULTIMAP, 
	/**
	 * Parameter is a path template variable; fills the URI path
	 */
	PATH,
	/**
	 * Parameter transmitted in a application/x-www-form-urlencoded request body
	 */
	FORM
	
}
