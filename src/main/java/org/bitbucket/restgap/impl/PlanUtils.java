/*
 * Copyright 2016 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.impl;

import java.lang.annotation.Annotation;

/**
 *
 * @author count
 */
public class PlanUtils {

	public static String appendPath(String baseUri, String path) {
		if (path == null) {
			return baseUri;
		}
		if (baseUri.endsWith("/") && path.startsWith("/")) {
			path = path.substring(1);
		} else if(!baseUri.endsWith("/") && !path.startsWith("/")) {
			baseUri = baseUri + '/';
		}
		return baseUri + path;
	}

	public static <T extends Annotation> T getAnnotation(Annotation[] annotations, Class<T> aClass) {
		for (Annotation a : annotations) {
			if (aClass.isInstance(a)) {
				return aClass.cast(a);
			}
		}
		return null;
	}
	
}
