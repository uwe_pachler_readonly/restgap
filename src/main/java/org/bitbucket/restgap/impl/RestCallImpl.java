/*
 * Copyright 2016 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.impl;

import java.lang.reflect.Type;
import java.util.List;
import org.bitbucket.restgap.client.RestCall;
import java.util.Map;
import org.bitbucket.restgap.Plan;

/**
 *
 * @author count
 */
public class RestCallImpl implements RestCall{
	
	private final Plan plan;
	private String uri;
	private String httpMethod;
	private final Map<String,List<Object>> httpHeaders;
	private Object httpBody;
	private Type responseType;
	private Map<String,List<Object>> queryParams;
	private Map<String,List<Object>> formParams;
	private Map<String, Object> pathParams;
	private final List<Runnable> cleanupHandlers;

	public RestCallImpl(Plan plan, String uri, String httpMethod, Map<String,List<Object>> headers, Object body, Type responseType, Map<String, Object> pathParams, Map<String,List<Object>> queryParams, Map<String,List<Object>> formParams, List<Runnable> cleanupHandlers) {
		this.plan = plan;
		this.uri = uri;
		this.httpMethod = httpMethod;
		this.httpHeaders = headers;
		this.httpBody = body;
		this.responseType = responseType;
		this.queryParams = queryParams;
		this.formParams = formParams;
		this.pathParams = pathParams;
		this.cleanupHandlers = cleanupHandlers;
	}

	public Plan getPlan() {
		return plan;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getHttpMethod() {
		return httpMethod;
	}

	public void setHttpMethod(String httpMethod) {
		this.httpMethod = httpMethod;
	}

	@Override
	public Object getHttpBody() {
		return httpBody;
	}

	@Override
	public void setHttpBody(Object body) {
		this.httpBody = body;
	}

	@Override
	public Map<String,List<Object>> getHeaders() {
		return httpHeaders;
	}
	
	@Override
	public Type getResponseType() {
		return responseType;
	}

	public void setResponseType(Class<?> responseType) {
		this.responseType = responseType;
	}

	@Override
	public Map<String, List<Object>> getQueryParams() {
		return queryParams;
	}
	
	public void setQueryParams(Map<String,List<Object>> queryParams) {
		this.queryParams = queryParams;
	}

	@Override
	public Map<String, List<Object>> getFormParams() {
		return formParams;
	}

	public void setFormParams(Map<String, List<Object>> formParams) {
		this.formParams = formParams;
	}
	
	@Override
	public Map<String, Object> getPathParams() {
		return pathParams;
	}

	public void setPathParams(Map<String, Object> pathParams) {
		this.pathParams = pathParams;
	}
	
	void cleanup() {
		for(Runnable r : cleanupHandlers) {
			r.run();
		}
	}
	
}
