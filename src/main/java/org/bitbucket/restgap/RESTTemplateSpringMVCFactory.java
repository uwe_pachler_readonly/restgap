/*
 * Copyright 2016 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap;

import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.bitbucket.restgap.backend.spring.RestTemplateBackend;
import static org.bitbucket.restgap.impl.PlanUtils.appendPath;
import org.bitbucket.restgap.backend.spring.RestTemplatePlanExecutor;
import org.bitbucket.restgap.frontend.Frontend;
import org.bitbucket.restgap.frontend.springmvc.SpringMVCFrontend;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.MatrixVariable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

/**
 * This factory bridges the gap between Spring MVC {@link RequestMapping}-annotated
 * interfaces and a {@link RestTemplate}-instance.
 * 
 * The factory will require you to use controller interfaces in Spring. So 
 * you'll have an interface like this:
 <pre>
 
 * &#64;Controller
 public interface BankServer {
    &#64;RequestMapping(path = "bank/{accountNumber}/balance", method = RequestMethod.GET, produces = "application/json")
    &#64;ResponseBody
	public float balance(&#64;PathParameter("account") String accountNumber);
 }
 * 
</pre>
 * 
 * ..with a server-side implementation like that:
 * 
 <pre>
 
 &#64;Controller
 public class BankServerImpl implements BankServer {
    &#64;Override
	public float balance(String accountNumber) {
	    // return whatever is in this account...
		...
	}
}

</pre>
 * 
 * 
 * <h2>Supported Features</h2>
 * 
 * {@linkplain RESTTemplateSpringMVCFactory} is intended for the REST aspect
 * of Spring MVC; the actual view aspect is not supported. Therefore, only
 * a small selection of {@link RequestMapping}'s parameter and return type
 * features are supported. These are:
 * 
 * <h3>Supported Parameter Features</h3>
 * All method parameters must be annotated with one of the following annotations:
 * <ul>
 * <li>{@link PathVariable} - for variable path segment like <code>/foo/{variable}/bar</code></li>
 * <li>{@link RequestParam} - to support URL query params like <code>?param=value&amp;param2=value2</code></li>
 * </ul>
 * We will support these in the future (currently unsupported):
 * <ul>
 * <li>{@link MatrixVariable}</li>
 * <li>{@link RequestHeader}</li>
 * <li>{@link RequestBody}</li>
 * <li>{@link RequestPart}</li>
 * </ul>
 * Other parameter features or parameters without annotations are not supported.
 * 
 * <h3>Supported Return Types</h3>
 * With return types, there is the choice between specifying the {@link ResponseBody}
 * annotation and/or returning a {@link ResponseEntity}:
 * <ul>
 * <li>{@link HttpEntity} (or {@link ResponseEntity}) to return the response directly</li>
 * <li>{@link ResponseBody} annotation to convert any return types to a HTTP response body using a message converter configured in the {@linkplain RestTemplate}</li>
 * </ul>
 * Most other features are unsupported because they're not REST related. Asynchronous
 * results may be suppored at a later stage.
 * 
 * 
 * @author uwe_pachler
 */
public final class RESTTemplateSpringMVCFactory {
	
	static Frontend frontend = new SpringMVCFrontend();
	
	private RESTTemplateSpringMVCFactory() {
	}
	
	// see JavaDoc of {@link RequestMapping} for a list of possible 
	// controller handler method return types. Many such return types belong
	// to the MVC infrastructure which is usually concerned with rendering
	// a Web-UI.
	//
	// We won't support many of these in the near future, because they make
	// the controller interface very server-heavy: A server-side controller
	// implemenentation will find a ModelAndView instance very useful;
	// a client receiving it won't be able to do much with it.
	//
	// For this reason there is no intention to support that kind of return
	// type in Spring MVC controller interfaces.
	public static <T> T create(RestTemplate rt, String baseUri, Class<T> clazz) {
		
		ClientFactory<T,RestTemplate> cf = ClientFactory.ofInterface(clazz)
			.baseUri(baseUri)
			.frontend(SpringMVCFrontend.instance())
			.backend(RestTemplateBackend.instance())
			.build();
		return cf.create(rt);
	}
	
}
