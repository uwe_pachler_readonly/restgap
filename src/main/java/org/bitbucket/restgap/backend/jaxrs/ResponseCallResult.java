/*
 * Copyright 2017 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.backend.jaxrs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ws.rs.core.Response;
import org.bitbucket.restgap.backend.RestCallResult;

/**
 *
 * @author upachler
 */
public class ResponseCallResult implements RestCallResult {
	private final Response response;

	public ResponseCallResult(Response response) {
		this.response = response;
	}
	
	
	@Override
	public int getStatusCode() {
		return response.getStatus();
	}

	@Override
	public Object getBody() {
		return response.getEntity();
	}
	
	@Override
	public Map<String, List<String>> getHeaders() {
		Map<String,List<String>> r = new HashMap<>();
		for(Map.Entry<String, List<Object>> e : response.getHeaders().entrySet()) {
			List<String> l = new ArrayList<>();
			for(Object o : e.getValue()) {
				l.add(o.toString());
			}
			r.put(e.getKey(), l);
		}
		return r;
	}

	public Response getResponse() {
		return response;
	}

	
}
