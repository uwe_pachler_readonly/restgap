/*
 * Copyright 2016 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap;

import org.bitbucket.restgap.impl.RestGapClientImpl;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.bitbucket.restgap.backend.PlanExecutor;
import org.bitbucket.restgap.client.CallHandler;
import org.bitbucket.restgap.client.ResultHandler;

/**
 *
 * @author count
 */
class PlanInvocationHandler extends RestGapClientImpl implements InvocationHandler {
	
	private final Class clazz;
	private final PlanExecutor executor;
	private final Map<String, Plan> plans;
	private final List<CallHandler> callHandlers = new ArrayList<>();
	private final List<ResultHandler> resultHandlers = new ArrayList<>();

	public PlanInvocationHandler(Class clazz, PlanExecutor executor, Map<String, Plan> callPlans) {
		this.clazz = clazz;
		this.executor = executor;
		this.plans = callPlans;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		String mkey = method.toGenericString();
		Plan plan = plans.get(mkey);
		if (plan == null) {
			throw new UnsupportedOperationException(String.format("the called method '%s' is not backed by a JAX-RS annotation", method.getName()));
		}
		
		return executor.execute(plan, this, args);
	}

	@Override
	public void addCallHandler(CallHandler callHandler) {
		callHandlers.add(callHandler);
	}

	@Override
	public void addResultHandler(ResultHandler resultHandler) {
		resultHandlers.add(0, resultHandler);
	}

	@Override
	public List getCallHandlers() {
		return callHandlers;
	}

	@Override
	public List getResultHandlers() {
		return resultHandlers;
	}
	
}
