/*
 * Copyright 2017 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.resources.jaxrs.serverimpl;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import org.bitbucket.restgap.resources.jaxrs.AuthenticatedResource;

/**
 *
 * @author upachler
 */
public class AuthenticatedResourceServer implements AuthenticatedResource{

	@Context
	HttpServletRequest hreq;
	private boolean lastCallSuccessful;
	
	@Override
	public void post() {
		String auth = hreq.getHeader("Authorization");
		if(auth == null) {
			lastCallSuccessful = false;
			throw new WebApplicationException(Response.Status.UNAUTHORIZED);
		}
		lastCallSuccessful = true;
	}

	public boolean lastCallSuccessful() {
		return lastCallSuccessful;
	}
	
}
