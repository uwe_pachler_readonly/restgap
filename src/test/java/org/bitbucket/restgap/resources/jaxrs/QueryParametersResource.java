/*
 * Copyright 2016 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.resources.jaxrs;

import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

/**
 *
 * @author count
 */
@Path("/queryparameters")
public interface QueryParametersResource {
	
	@GET
	@Path("/int")
	public void queryInt(@QueryParam("value") int value);
	
	@GET
	@Path("/float")
	public void queryFloat(@QueryParam("value") float value);
	
	@GET
	@Path("/boolean")
	public void queryBoolean(@QueryParam("value") boolean value);
	
	@GET
	@Path("/strings")
	public void queryStrings(@QueryParam("value") List<String> value);
}
