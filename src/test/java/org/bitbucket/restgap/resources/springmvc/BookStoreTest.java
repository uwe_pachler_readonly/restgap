/*
 * Copyright 2016 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.resources.springmvc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.servlet.Servlet;
import org.bitbucket.restgap.JettyTestUtil;
import static org.bitbucket.restgap.JettyTestUtil.shutdownJetty;
import static org.bitbucket.restgap.JettyTestUtil.startupJetty;
import org.bitbucket.restgap.RESTTemplateSpringMVCFactory;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.springframework.web.client.HttpClientErrorException;

/**
 *
 * @author count
 */
public class BookStoreTest {
	static JettyTestUtil.JettySetup setup;
	
	@BeforeClass
	public static void startup() throws Exception {
		setup = startupJetty("WEB-INF/applicationContext-springmvc-bookStoreTest.xml", new JettyTestUtil.ServletFactory() {
			@Override
			public Servlet createServlet(WebApplicationContext ctx) {
				return new DispatcherServlet(ctx);
			}
		});
	}
	
	@AfterClass
	public static void shutdown() throws Exception {
		shutdownJetty(setup);
	}
	
	@Test
	public void testLoadDefaultBook() {
		BookStore bookStore = RESTTemplateSpringMVCFactory.create(rt(), baseUri(), BookStore.class);
		Book defaultBook = bookStore.load(BookStore.DEFAULT_BOOK_ID);
		Assert.assertNotNull(defaultBook);
	}
	
	@Test
	public void testSearchBook() {
		BookStore bookStore = RESTTemplateSpringMVCFactory.create(rt(), baseUri(), BookStore.class);
		List<Book> books = bookStore.find("Dëf@ult");
		Assert.assertEquals(false, books.isEmpty());
		
		Book fiftyPercentBook = new Book();
		fiftyPercentBook.setTitle("50% Of Nothing: %00!");
		fiftyPercentBook.setAuthor("Mr. Percent");
		String PERCENTBOOK_ID = "50%Book";
		bookStore.store(PERCENTBOOK_ID, fiftyPercentBook);
		
		Assert.assertNotNull(bookStore.find("%00"));
	}
	
	@Test
	public void testRequestBody() {
		BookStore bookStore = RESTTemplateSpringMVCFactory.create(rt(), baseUri(), BookStore.class);
		
		Book in = addMickeyMouseBook(bookStore);
		
		Book out = bookStore.load(in.getId());
		
		Assert.assertEquals(in, out);
	}
	
	private Book addMickeyMouseBook(BookStore bookStore) {
		final String BOOK_ID = "my-first-book";

		Book in = new Book();
		in.setTitle("Mickey Mouse");
		in.setAuthor("Walt Disney");
		
		bookStore.store(BOOK_ID, in);
		in.setId(BOOK_ID);
		return in;
	}
	
	@Test
	public void testBuy() {
		BookStore bookStore = RESTTemplateSpringMVCFactory.create(rt(), baseUri(), BookStore.class);
		List<Book> books = bookStore.find("Dëf@ult");
		
		bookStore.purchase(Arrays.asList(books.get(0).getId()));
		
		addMickeyMouseBook(bookStore);
		
		books = bookStore.find("");
		
		List<String> ids = new ArrayList<>(books.size());
		for(Book book : books) {
			ids.add(book.getId());
		}
		
		List<Book> purchasedBooks = bookStore.purchase(ids);
		assertEquals(ids.size(), purchasedBooks.size());
		for(Book book : purchasedBooks) {
			assertTrue(ids.contains(book.getId()));
		}
	}
	
	@Test
	public void testAddDelete() {
		BookStore bookStore = RESTTemplateSpringMVCFactory.create(rt(), baseUri(), BookStore.class);
		Book mmBook = addMickeyMouseBook(bookStore);
		
		// test deleting the book
		bookStore.delete(mmBook.getId());
		
		boolean exceptionThrown;
		try {
			bookStore.load(mmBook.getId());
			exceptionThrown = false;
		} catch(HttpClientErrorException hcex) {
			exceptionThrown = true;
		}
		assertTrue(exceptionThrown);
	}
	
	@Test
	public void testContact() {
		BookStore bookStore = RESTTemplateSpringMVCFactory.create(rt(), baseUri(), BookStore.class);
		
		String EMAIL = "me@here.com";
		String MESSAGE = "hello? helloo-oo?";
		String response = bookStore.contact(EMAIL, MESSAGE);
		
		assertEquals(String.format(BookStore.THANKS_FOR_CONTACTING_FORMAT, EMAIL, MESSAGE), response);
		
	}
	
	private RestTemplate rt() {
		return new RestTemplate();
	}
	
	private String baseUri() {
		return setup.getBaseUri().toASCIIString();
	}
}
