/*
 * Copyright 2016 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.resources.springmvc.serverimpl;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import org.bitbucket.restgap.resources.springmvc.Book;
import org.bitbucket.restgap.resources.springmvc.BookStore;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author count
 */
@Controller
public class BookStoreServer implements BookStore{
	
	Map<String, Book> books = new HashMap<>();
	private static Locale LOCALE = Locale.US;
	
	{
		Book defaultBook = new Book();
		defaultBook.setAuthor("Mr. Dëf@ult");
		defaultBook.setTitle("Alone @ Home");
		defaultBook.setId(DEFAULT_BOOK_ID);
		books.put(DEFAULT_BOOK_ID, defaultBook);
	}
	@Override
	public synchronized void store(@PathVariable("bookId") String bookId, @RequestBody Book book) {
		book.setId(bookId);
		books.put(bookId, book);
		
	}

	class BookStoreServerException extends RuntimeException{
		final HttpStatus status;
		BookStoreServerException(HttpStatus status) {
			this.status = status;
		}
		HttpStatus getStatus(){
			return status;
		}
	}
	
	@ExceptionHandler
	void exceptionHandler(BookStoreServerException bssx, HttpServletResponse hres) {
		hres.setStatus(bssx.getStatus().value());
	}
	
	@Override
	public Book load(@PathVariable("bookId") String bookId) {
		Book book = books.get(bookId);
		if(book == null) {
			throw new BookStoreServerException(HttpStatus.NOT_FOUND);
		}
		return book;
	}

	@Override
	public void delete(@PathVariable("bookId") String bookId) {
		books.remove(bookId);
	}

	@Override
	public synchronized List<Book> find(@RequestParam("search") String search) {
		String searchLC = search.toLowerCase(LOCALE);
		List<Book> result = new ArrayList<>();
		for(Book b : books.values()) {
			String titleLC = b.getTitle().toLowerCase(LOCALE);
			if(titleLC.contains(searchLC)) {
				result.add(b);
				continue;
			}
			String authorLC = b.getAuthor().toLowerCase(LOCALE);
			if(authorLC.contains(searchLC)) {
				result.add(b);
			}
		}
		return result;
	}

	@Override
	public List<Book> purchase(@RequestParam("id") List<String> bookIds) {
		List<Book> purchasedBooks = new ArrayList<>();
		for(String bookId : bookIds) {
			Book b = books.get(bookId);
			if(b != null) {
				purchasedBooks.add(b);
			}
		}
		return purchasedBooks;
	}

	@Override
	public String contact(String email, String message) {
		return String.format(THANKS_FOR_CONTACTING_FORMAT, email, message);
	}

	
}
