/*
 * Copyright 2016 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.resources.springmvc;

import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author count
 */
public interface BookStore {
	
	public static String DEFAULT_BOOK_ID = "$DEFAULT";
	public static String THANKS_FOR_CONTACTING_FORMAT = "Thanks %s for contacting us! Your Message was: %s";
	
	@RequestMapping(
		path = "/books/{bookId}",
		method = RequestMethod.PUT,
		consumes = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseStatus(value = HttpStatus.OK)
	public void store(@PathVariable("bookId") String bookId, @RequestBody Book book);
	
	@RequestMapping(
		path = "/books/{bookId}",
		method = RequestMethod.DELETE
	)
	@ResponseStatus(value = HttpStatus.OK)
	public void delete(@PathVariable("bookId") String bookId);
	
	@RequestMapping(
		path = "/books/{bookId}",
		method = RequestMethod.GET,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody 
	public Book load(@PathVariable("bookId") String bookId);
	
	@RequestMapping(
		path = "/books",
		method = RequestMethod.GET,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody 
	public List<Book> find(@RequestParam("search") String search);
	
	@RequestMapping(
		path = "/books/buy",
		method = RequestMethod.POST,
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@ResponseBody
	public List<Book> purchase(@RequestParam("id") List<String> bookIds);
	
	@RequestMapping(
		path = "/contact",
		method = RequestMethod.POST,
		consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
		produces = "text/plain"
	)
	@ResponseBody
	public String contact(@RequestParam("email") String email, @RequestParam("message") String message);
	
}
