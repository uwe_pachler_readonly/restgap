/*
 * Copyright 2016 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap.resources.springmvc;

import java.net.URI;
import javax.servlet.Servlet;
import static org.bitbucket.restgap.JettyTestUtil.*;
import org.bitbucket.restgap.RESTTemplateSpringMVCFactory;
import org.bitbucket.restgap.resources.springmvc.ReturnTypes;
import org.bitbucket.restgap.resources.springmvc.serverimpl.ReturnTypesServer;
import org.eclipse.jetty.server.ServerConnector;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

/**
 *
 * @author upachler
 */
public class ReturnTypesTest {
	static JettySetup setup;
	
	@BeforeClass
	public static void startup() throws Exception {
		setup = startupJetty("WEB-INF/applicationContext-springmvc-returnTypesTest.xml", new ServletFactory() {
			@Override
			public Servlet createServlet(WebApplicationContext ctx) {
				return new DispatcherServlet(ctx);
			}
		});
	}
	
	@AfterClass
	public static void shutdown() throws Exception {
		shutdownJetty(setup);
	}
	
	URI baseUri() {
		return setup.getBaseUri();
	}
	
	@Test
	public void testRaw() throws Exception {
		ReturnTypes ret = RESTTemplateSpringMVCFactory.create(rt(), baseUri().toString(), ReturnTypes.class);
		String result = ret.returnRaw();

		Assert.assertEquals(ReturnTypesServer.RESPONSE_VALUE, result);
	}
	
	@Test
	public void testResponseEntity() throws Exception {
		ReturnTypes ret = RESTTemplateSpringMVCFactory.create(rt(), baseUri().toString(), ReturnTypes.class);
		ResponseEntity<String> result = ret.returnResponseEntity();
		
		Assert.assertEquals(HttpStatus.OK, result.getStatusCode());
		Assert.assertEquals(ReturnTypesServer.RESPONSE_VALUE, result.getBody());
	}
	
	@Test
	public void testInt() {
		ReturnTypes ret = RESTTemplateSpringMVCFactory.create(rt(), baseUri().toString(), ReturnTypes.class);
		int result = ret.returnInt();
		
		Assert.assertEquals(ReturnTypesServer.RESPONSE_INT, result);
	}
	
	private RestTemplate rt() {
		return new RestTemplate();
	}
	
}
