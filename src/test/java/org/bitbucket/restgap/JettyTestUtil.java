/*
 * Copyright 2016 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap;

import java.net.URI;
import javax.servlet.Servlet;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.XmlWebApplicationContext;

/**
 * org.springframework.web.servlet.DispatcherServlet
 * @author count
 */
public abstract class JettyTestUtil {
	public interface ServletFactory {
		public Servlet createServlet(WebApplicationContext ctx);
	}
	
	public static final class JettySetup {
		private final Server server;
		private final WebApplicationContext springContext;

		public JettySetup(Server server, WebApplicationContext springContext) {
			this.server = server;
			this.springContext = springContext;
		}

		
		public Server getServer() {
			return server;
		}

		public WebApplicationContext getSpringContext() {
			return springContext;
		}

		public URI getBaseUri() {
			int port = ((ServerConnector)(getServer().getConnectors()[0])).getLocalPort();
			return URI.create("http://localhost:" + port + "/");
		}
	}
	// ideas from http://kielczewski.eu/2013/11/using-embedded-jetty-spring-mvc/
	public static JettySetup startupJetty(String configLocation, ServletFactory servletFactory) throws Exception {
		Server server = new Server(0);

		// Spring's application context. By default, the spring config is loaded
		// from WEB-INF/applicationContext.xml, but we'll configure it to load
		// the given configuration file, specific to the test
		XmlWebApplicationContext ctx = new XmlWebApplicationContext();
		ctx.setConfigLocation(configLocation);
		
		ServletContextHandler chandler = new ServletContextHandler();

		// only one webapp; we serve from the root
		chandler.addServlet(new ServletHolder(servletFactory.createServlet(ctx)), "/*");

		// Lifecycle events propagate to Spring
		chandler.addEventListener(new ContextLoaderListener(ctx));

		// All resources are taken from the root of the classpath (this is fine for testing)
		chandler.setResourceBase(new ClassPathResource("/").getURI().toString());

		// DispatcherServlet should handle errors, not Jetty
		chandler.setErrorHandler(null);

		server.setHandler(chandler);

		server.start();
		
		return new JettySetup(server, ctx);
	}
	
	public static void shutdownJetty(JettySetup setup) throws Exception {
		Server server = setup.getServer();
		if(server.isStarting()) {
			server.join();
		}

		if(server.isStarted()) {
			server.stop();
		}
	}
	
	
}
