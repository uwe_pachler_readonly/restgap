/*
 * Copyright 2016 The RESTGap Project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.restgap;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.Base64;
import org.bitbucket.restgap.resources.jaxrs.NonParameterizedResource;
import org.bitbucket.restgap.resources.jaxrs.PathParametersResource;
import org.junit.Test;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.client.RequestMatcher;
import org.springframework.web.client.RestTemplate;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.*;
import static org.springframework.test.web.client.response.MockRestResponseCreators.*;
import org.bitbucket.restgap.resources.jaxrs.QueryParametersResource;
import org.springframework.http.HttpMethod;

/**
 *
 * @author count
 */
public class RESTTemplateJAXRSClientFactoryTest {
	
	private static final URI BASE_URI = URI.create("http://testhost/");
	
	public RESTTemplateJAXRSClientFactoryTest() {
	}
	
	private RequestMatcher requestPath(String path) {
		return requestTo(BASE_URI.resolve(path));
	}
	/**
	 * Testing query parameters
	 */
	@Test
	public void testQueryParameters() {
		RestTemplate rt = rt();
		MockRestServiceServer mrss = mrss(rt);
		
		QueryParametersResource instance = RESTTemplateJAXRSClientFactory.create(rt, BASE_URI.toString(), QueryParametersResource.class);
		
		mrss
				.expect(requestPath("/queryparameters/int?value=42"))
				.andRespond(withSuccess());
		instance.queryInt(42);
		mrss.verify();
		
		
		mrss = mrss(rt);
		mrss
				.expect(requestPath("/queryparameters/float?value=42.5"))
				.andRespond(withSuccess());
		instance.queryFloat(42.5f);
		mrss.verify();
		
		mrss = mrss(rt);
		mrss
				.expect(requestPath("/queryparameters/boolean?value=true"))
				.andRespond(withSuccess());
		instance.queryBoolean(true);
		mrss.verify();
	}

	/**
	 * Testing path parameters
	 */
	@Test
	public void testPathParameters() {
		RestTemplate rt = rt();
		MockRestServiceServer mrss = mrss(rt);
		
		PathParametersResource instance = RESTTemplateJAXRSClientFactory.create(rt, BASE_URI.toString(), PathParametersResource.class);
		
		mrss
				.expect(requestPath("/pathparameters/int/42"))
				.andExpect(method(HttpMethod.GET))
				.andRespond(withSuccess());
		
		instance.getPathInt(42);
		mrss.verify();
	}
	
	@Test
	public void testAuthorization() throws UnsupportedEncodingException {
		
		final String username = "Aladdin";
		final String password = "open sesame";
		
		String auth = Base64.getEncoder().encodeToString(("Basic " + username + ':' + password).getBytes("ISO-8859-1"));
		RestTemplate rt = rt();
		MockRestServiceServer mrss = mrss(rt);
		
		NonParameterizedResource instance = RESTTemplateJAXRSClientFactory.create(rt, BASE_URI.toString(), NonParameterizedResource.class, username, password);
		
		mrss
			.expect(requestPath("/no-params"))
			.andExpect(method(HttpMethod.POST))
			.andExpect(header("Authorization", auth))
			.andRespond(withSuccess());
			;
		
		instance.post();
		
		mrss.verify();
	}

	private RestTemplate rt() {
		return new RestTemplate();
	}
	
	private MockRestServiceServer mrss(RestTemplate rt) {
		MockRestServiceServer mrss = MockRestServiceServer.createServer(rt);
		return mrss;
	}
	
}
